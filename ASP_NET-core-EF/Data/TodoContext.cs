﻿using ASP_NET_core_EF.Models;
using Microsoft.EntityFrameworkCore;

namespace ASP_NET_core_EF.Data
{
    public class TodoContext : DbContext
    {
        public TodoContext(DbContextOptions<TodoContext> options)
          : base(options)
        {
            Database.EnsureCreated();
        }


        public DbSet<TodoItem> TodoItems { get; set; }
        public DbSet<TodoItemDTO> TodoItemDTOs { get; set; }
    }
}
